#include "Triangle.h"

double Triangle::getArea() const
{
	//take the points of the triangle
	Point a = _points[0];
	Point b = _points[1];
	Point c = _points[2];

	double distance1 = b.distance(a);

	double distance2 = c.distance(b);

	double distance3 = c.distance(a);

	double s = (distance1 + distance2 + distance3) / 2;
	return sqrt(s * (s - distance1) * (s - distance2) * (s - distance3));
}

double Triangle::getPerimeter() const
{
	//take the points of the triangle
	Point a = _points[0];
	Point b = _points[1];
	Point c = _points[2];

	double distance1 = a.distance(b);
	double distance2 = a.distance(c);
	double distance3 = c.distance(b);

	double perimeter = distance1 + distance2 + distance3;


	return perimeter;
}

void Triangle::move(const Point& other)
{
	for (int i = 0; i < 3; i++)
	{
		//moved x and y in pos i
		_points[i] = _points[i].operator+=(other); //_points[i].operator+=(other);
	}
}

void Triangle::draw(const Canvas& canvas)
{
	canvas.draw_triangle(_points[0], _points[1], _points[2]);
}

void Triangle::clearDraw(const Canvas& canvas)
{
	canvas.clear_triangle(_points[0], _points[1], _points[2]);
}

Triangle::Triangle(const Point& a, const Point& b, const Point& c, const std::string& type, const std::string& name) :
	Polygon(type, name)
{
	_points.push_back(a);
	_points.push_back(b);
	_points.push_back(c);
}

Triangle::~Triangle()
{
}
