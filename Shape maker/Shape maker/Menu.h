#pragma once
#include "Shape.h"
#include <vector>
#include "Save_detail.h"
#include "Canvas.h"
#include "Arrow.h"
#include "Canvas.h"
#include "Circle.h"
#include "CImg.h"
#include "Point.h"
#include "Polygon.h"
#include "Rectangle.h"

//helper
int getNumChoice();

class Menu
{
public:

	Menu();
	~Menu();

	int start() const;
	void newShape(Save_detail& save_shapes);
	void do_circal(Save_detail& save_shapes);
	void do_arrow(Save_detail& save_shapes);
	void do_triangle(Save_detail& save_shapes);
	void do_rectangle(Save_detail& save_shapes);

	void modify(Save_detail& save_shapes);

	void delete_all(Save_detail& save_shapes);

private: 
	Canvas _canvas;
};

