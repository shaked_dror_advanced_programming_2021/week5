#include "Menu.h"
#include "Shape.h"
#include "Canvas.h"
#include "Arrow.h"
#include "Canvas.h"
#include "Circle.h"
#include "CImg.h"
#include "Point.h"
#include "Polygon.h"
#include "Rectangle.h"
//#include "Triangle.h"
#include "Save_detail.h"
#include <vector>

#define NEW_SHAPE 0
#define GET_INFO 1
#define DELETE 2

void main()
{
	Menu progrem = Menu();
	Save_detail save_shapes;
	int choice = 0;
	while (choice != 3)
	{
		system("cls");
		choice = progrem.start();
		if (choice == NEW_SHAPE)
		{
			progrem.newShape(save_shapes);
		}
		if (choice == GET_INFO)
		{
			progrem.modify(save_shapes);
		}
		if (choice == DELETE)
		{
			progrem.delete_all(save_shapes);
		}
	}
	 
}