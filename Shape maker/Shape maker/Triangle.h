#include "Polygon.h"
#include <string>
#include <vector>

class Triangle : public Polygon
{
public:
	Triangle(const Point& a, const Point& b, const Point& c, const std::string& type, const std::string& name);//*
	virtual ~Triangle();//*

	void draw(const Canvas& canvas);//*
	void clearDraw(const Canvas& canvas);//*

	// override functions if need (virtual + pure virtual)
	double getArea() const; //*
	double getPerimeter() const;//*
	void move(const Point& other);//*
};