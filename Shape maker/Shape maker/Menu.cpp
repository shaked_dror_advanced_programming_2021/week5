#include "Menu.h"

//helper
//get a number in from the user (from start to end)
int getNumChoice()
{
    int x;
    std::cin >> x;
    while ((std::cin.fail()))
    {
        std::cin.clear();
        std::cin.ignore();
        std::cout << "Bad entry. Enter a new NUMBER : ";
        std::cin >> x;
        std::cout << std::endl;
    }
    return x;
}

//////////////////////////////////////////////////////////////////////////////


Menu::~Menu()
{
	//?
}

Menu::Menu()
{
	//?
}

//will do the start msg and start choice
int Menu::start() const
{
	std::cout << "Enter 0 to add a new shape.\n"
		         "Enter 1 to modify or get information from a current shape.\n"
		         "Enter 2 to delete all of the shapes.\n"
		         "Enter 3 to exit." << std::endl;

    return getNumChoice();
}

void Menu::newShape(Save_detail& save_shapes)
{
    std::cout << "Enter 0 to add a circle.\n"
                 "Enter 1 to add an arrow.\n"
                 "Enter 2 to add a triangle.\n"
                 "Enter 3 to add a rectangle." << std::endl;

    int choice = getNumChoice();


    if (choice == 0)
    {
        this->do_circal(save_shapes);
    }
    if (choice == 1)
    {
        
        this->do_arrow(save_shapes);
    }
    if (choice == 2)
    {
        this->do_triangle(save_shapes);
    }
    if (choice == 3)
    {
        this->do_rectangle(save_shapes);
    }

}

void Menu::do_circal(Save_detail& save_shapes)
{
    std::cout << "Please enter X:" << std::endl;
    int x;
    std::cin >> x;

    std::cout << "Please enter Y:" << std::endl;
    int y;
    std::cin >> y;
    Point center = Point(x, y);

    std::cout << "Please enter radius :" << std::endl;
    int r;
    std::cin >> r;

    std::cout << "Please enter the name of the shape:" << std::endl;
    std::string name;
    std::cin >> name;
    Circle *c = new Circle(center, r, "Circle", name);
    save_shapes.add_Shape(c);

    c->draw(_canvas);

}

void Menu::do_arrow(Save_detail& save_shapes)
{
    std::cout << "Enter the X of point number: 1" << std::endl;
    int x1;
    std::cin >> x1;

    std::cout << "Enter the Y of point number: 1" << std::endl;
    int y1;
    std::cin >> y1;
    Point p1 = Point(x1, y1);

    std::cout << "Enter the X of point number: 2" << std::endl;
    int x2;
    std::cin >> x2;

    std::cout << "Enter the Y of point number: 2" << std::endl;
    int y2;
    std::cin >> y2;
    Point p2 = Point(x2, y2);

    std::cout << "Please enter the name of the shape:" << std::endl;
    std::string name;
    std::cin >> name;

    Arrow *a = new Arrow(p1, p2, "Arrow", name);
    save_shapes.add_Shape(a);
    a->draw(_canvas);
}

void Menu::do_triangle(Save_detail& save_shapes)
{
    Triangle* t;
    std::cout << "Enter the X of point number: 1" << std::endl;
    int x1;
    std::cin >> x1;

    std::cout << "Enter the Y of point number: 1" << std::endl;
    int y1;
    std::cin >> y1;
    Point p1 = Point(x1, y1);

    std::cout << "Enter the X of point number: 2" << std::endl;
    int x2;
    std::cin >> x2;

    std::cout << "Enter the Y of point number: 2" << std::endl;
    int y2;
    std::cin >> y2;
    Point p2 = Point(x2, y2);

    std::cout << "Enter the X of point number: 3" << std::endl;
    int x3;
    std::cin >> x3;

    std::cout << "Enter the Y of point number: 3" << std::endl;
    int y3;
    std::cin >> y3;
    Point p3 = Point(x3, y3);

    std::cout << "Please enter the name of the shape:" << std::endl;
    std::string name;
    std::cin >> name;

    //will save the shape and will draw the shape
    if (x1 == x2 || x1 == x3 || x2 == x3 || y1 == y2 || y1 == y3 || y2 == y3)
    {
        throw "triangle cann't be a line";
    }
    else
    {
        t = new Triangle(p1, p2, p3, "Triangle", name);
        save_shapes.add_Shape(t);
        t->draw(_canvas);
        //_canvas.draw_triangle(p1, p2, p3);
    }
}

void Menu::do_rectangle(Save_detail& save_shapes)
{
    //get the top left point from user
    std::cout << "Enter the X of the to left corner:" << std::endl;
    int x1;
    std::cin >> x1;

    std::cout << "Enter the Y of the to left corner:" << std::endl;
    int y1;
    std::cin >> y1;
    Point p1 = Point(x1, y1);

    //get length
    std::cout << "Please enter the length of the shape:" << std::endl;
    int length;
    std::cin >> length;
    //get width
    std::cout << "Please enter the width of the shape:" << std::endl;
    int width;
    std::cin >> width;

    std::cout << "Please enter the name of the shape:" << std::endl;
    std::string name;
    std::cin >> name;

    if (length < 0 || width < 0)
    {
        throw "length or width cann't be 0 or below";
    }
    else
    {
        //will save the shape and will draw the shape
        myShapes::Rectangle *rec = new myShapes::Rectangle(p1, length, width, "Rectangle", name);
        save_shapes.add_Shape(rec);
        rec->draw(_canvas);
    }
}


void Menu::modify(Save_detail& save_shapes)
{
    for (int i = 0; i < save_shapes._shape.size(); i++)
    {
        std::cout << "Enter " << i << " for ";
        save_shapes._shape[i]->printDetails();
    }
    if(save_shapes._shape.size() == 0)
    {
        std::cout << "no shapes  found. Enter a number to return to the menu: " << std::endl;
    }
    int choice = getNumChoice();

    if (choice > save_shapes._shape.size() || choice < 0)
    {
        if (save_shapes._shape.size() != 0)
        {
            std::cout << "out of index" << std::endl;
        }    
    }
    else
    {
        std::cout << "Enter 0 to move the shape\n"
            "Enter 1 to get its details.\n"
            "Enter 2 to remove the shape." << std::endl;
        int to_do = getNumChoice();

        if (to_do == 0)
        {
            std::cout << "Please enter X:" << std::endl;
            int x;
            std::cin >> x;

            std::cout << "Please enter Y:" << std::endl;
            int y;
            std::cin >> y;
            Point p = Point(x, y);

            
            save_shapes._shape[choice]->clearDraw(_canvas);//will remove the shape from the canvas
            save_shapes._shape[choice]->move(p);// will move the shape
            save_shapes._shape[choice]->draw(_canvas);// will updata the shape in the canvas 
        }
        else if (to_do == 1)
        {
            std::cout << save_shapes._shape[choice]->getType() << "  " << save_shapes._shape[choice]->getName()
            << "    " << save_shapes._shape[choice]->getPerimeter() << "  " << save_shapes._shape[choice]->getArea() << std::endl;
            
        }
        else if (to_do == 2)
        {
            save_shapes._shape[choice]->clearDraw(_canvas);// clean the shape from the canvas
            int t = 0;
            for (int i = 0; i < save_shapes._shape.size(); i++)// will updata the vector
            {
                if (i != choice)
                {
                    save_shapes._shape[t] = save_shapes._shape[i];
                    t++;
                }
            }
            save_shapes._shape.pop_back();

            for (int i = 0; i < save_shapes._shape.size(); i++)// will fix the canvas
            {
                save_shapes._shape[i]->draw(_canvas);
            }

        }
    }
   


    std::cout << std::endl;
}

void Menu::delete_all(Save_detail& save_shapes)
{
   for (int i = 0; i < save_shapes._shape.size(); i++)//will clean the canvas
    {
        save_shapes._shape[i]->clearDraw(_canvas);
        
    }
   for (int i = 0; i < save_shapes._shape.size(); i++)// will remove all the shapes
   {
       save_shapes._shape.pop_back();
   } 
}

