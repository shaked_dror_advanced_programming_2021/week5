#include "Point.h"

Point::Point(double x, double y)
{
	this->_x = x;
	this -> _y = y;
}

Point::Point(const Point& other)
{
	this->_x = other._x;
	this->_y = other._y;
}

Point Point::operator+(const Point& other) const
{
	Point p2 = *this; // copying this vector using copy constructor
	p2 += other;		 // use substruction assignment operator to substruct other from result
	return p2;
}

Point& Point::operator+=(const Point& other)
{
	this->_x += other._x;
	this->_y += other._y;

	return *this;
}

double Point::getY() const
{
	return this->_y;
}

double Point::getX() const
{
	return this->_x;
}

double Point::distance(const Point& other) const
{
	double y = this->_y - other._y;
	double x = this->_x - other._x;
	double d = x * x + y * y;
	return sqrt(d);
}


Point::~Point()
{
}
