#include "Arrow.h"

Arrow::Arrow(const Point& a, const Point& b, const std::string& type, const std::string& name) :
	Shape(name, type)
{
	_points.push_back(a);
	_points.push_back(b);
}

Arrow::~Arrow()
{
}

void Arrow::draw(const Canvas& canvas)
{
	canvas.draw_arrow(_points[0], _points[1]);
}
void Arrow::clearDraw(const Canvas& canvas)
{
	canvas.clear_arrow(_points[0], _points[1]);
}

double Arrow::getArea() const
{
	return 0.0;
}

double Arrow::getPerimeter() const
{
	return _points[0].distance(_points[1]);
}

void Arrow::move(const Point& other)
{
	for (int i = 0; i < 2; i++)
	{
		//moved x and y in pos i
		_points[i] = _points[i].operator+=(other); //_points[i].operator+=(other);
	}
}

