#pragma once
#include "Polygon.h"


namespace myShapes
{
	// Calling it MyRectangle becuase Rectangle is taken by global namespace.
	class Rectangle : public Polygon
	{
	public:
		// There's a need only for the top left corner 
		Rectangle(const Point& a, double length, double width, const std::string& type, const std::string& name);//*
		virtual ~Rectangle();//*

		void draw(const Canvas& canvas);//*
		void clearDraw(const Canvas& canvas);//*

		double getArea() const;//*
		double getPerimeter() const;//*
		void move(const Point& other);//*

	};
}