#include "Shape.h"


Shape::Shape(const std::string& name, const std::string& type)
{
	this->_name = name;
	this->_type = type;
}

std::string Shape::getType() const
{
	return this->_type;
}

std::string Shape::getName() const
{
	return this->_name;
}

void Shape::printDetails() const
{
	std::cout << this->_type << "(" << this->_name << ")" << std::endl;
}


Shape::~Shape()
{
	//do not need to do 
}


