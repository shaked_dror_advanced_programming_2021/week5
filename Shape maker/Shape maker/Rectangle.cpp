#include "Rectangle.h"

myShapes::Rectangle::Rectangle(const Point& a, double length, double width, const std::string& type, const std::string& name) :
	Polygon(type, name)
{
	double x = a.getX();
	double y = a.getY();
	_points.push_back(a);//top left[0]
	_points.push_back(Point(x, y - width));//buttom left[1]
	_points.push_back(Point(x + length, y));//top rigth [2]
	_points.push_back(Point(x + length, y - width));//buttom rigth[3] 
}

myShapes::Rectangle::~Rectangle()
{
}

void myShapes::Rectangle::draw(const Canvas& canvas)
{
	canvas.draw_rectangle(_points[0], _points[3]);
}

void myShapes::Rectangle::clearDraw(const Canvas& canvas)
{
	canvas.clear_rectangle(_points[0], _points[3]);
}

double myShapes::Rectangle::getArea() const
{
	//the Rectangle points
	Point a = _points[0];
	Point b = _points[1];
	Point c = _points[2];
	Point d = _points[3];

	double width = a.distance(b);
	double length = a.distance(c);
	
	return (width * length);
}

double myShapes::Rectangle::getPerimeter() const
{
	//the Rectangle points
	Point a = _points[0];
	Point b = _points[1];
	Point c = _points[2];
	Point d = _points[3];

	double width = a.distance(b);
	double length = a.distance(c);

	double perimeter = width * 2 + length * 2;

	return perimeter;
}

void myShapes::Rectangle::move(const Point& other)
{
	for (int i = 0; i < 4; i++)
	{
		//moved x and y in pos i
		_points[i] = _points[i].operator+=(other);//_points[i].operator+=(other);
	}
}


