#pragma once

#include "Shape.h"
#include "Point.h"

#define PI 3.14

class Circle : public Shape
{
private:
	Point _center;
	double _radius;

public:
	Circle(const Point& center, double radius, const std::string& type, const std::string& name);//*
	~Circle();//*

	const Point& getCenter() const;//*
	double getRadius() const;//*

	virtual void draw(const Canvas& canvas);//*
	virtual void clearDraw(const Canvas& canvas);//*

	double getArea() const;//*
	double getPerimeter() const;//*
	void move(const Point& other);//*

};